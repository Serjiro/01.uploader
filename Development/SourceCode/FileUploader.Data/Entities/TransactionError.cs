﻿using System;

namespace FileUploader.Data.Entities
{
    public class TransactionError
    {
        public int TransactionErrorId { get; set; }
        public string Filename { get; set; }
        public string FileContent { get; set; }
        public string Errors { get; set; }
        public DateTime ServerDate { get; set; }
    }
}
