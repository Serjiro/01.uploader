﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileUploader.Data.Entities
{
    public class Transaction
    {       
        public string TransactionId { get; set; }
        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }
        public DateTime TransactionDate { get; set; }
        public byte Status { get; set; }
    }
}
