﻿using Microsoft.EntityFrameworkCore;
using FileUploader.Data.Entities;
using Microsoft.Extensions.Configuration;
using System.Configuration;

namespace FileUploader.Data
{
    public class TransactionContext : DbContext
    {
        public TransactionContext() { }
        public TransactionContext(DbContextOptions option) : base(option) { }

        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<TransactionError> TransactionErrors { get; set; }

        public static string ConnectionString {get;set;}

        public static TransactionContext GetContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<TransactionContext>();
            optionsBuilder.UseSqlServer(ConnectionString);

            TransactionContext dbContext = new TransactionContext(optionsBuilder.Options);
            return dbContext;
        }
    }
}
