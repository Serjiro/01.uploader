﻿using FileUploader.Data;
using FileUploader.Data.Entities;
using Microsoft.VisualBasic.FileIO;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FileUploader.Business
{
    public class FileUploaderCSV : IProcessFile
    {
        private Stream _stream;
        public FileUploaderCSV(Stream stream)
        {
            _stream = stream;
        }

        public List<string> ErrorData { get; private set; }
        public List<UploadData> UploadedList { get; private set; }

        public bool Process()
        {
            var csvReader = new TextFieldParser(_stream);
            var delimeter = ",";
            csvReader.SetDelimiters(new string[] { delimeter });
            csvReader.HasFieldsEnclosedInQuotes = true;
            var success = true;

            ErrorData = new List<string>();
            UploadedList = new List<UploadData>();
            while (!csvReader.EndOfData)
            {
                var colFields = csvReader.ReadFields();
                var fileUploadDataProcessor = new FileUploadDataProcessor(colFields, delimeter);

                if (fileUploadDataProcessor.ErrorList.Count() > 0)
                {
                    ErrorData.AddRange(fileUploadDataProcessor.ErrorList);
                    success = false;
                }
                else
                {
                    if (success && fileUploadDataProcessor.UploadData != null)
                    {
                        UploadedList.Add(fileUploadDataProcessor.UploadData);
                    }
                }
            }

            return success;
        }

    }


}
