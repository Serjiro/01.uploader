﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileUploader.Business
{
    public enum FileUploadStatusCSV : byte
    {
        Approved,
        Failed,
        Finished
    }

    public enum FileUploadStatusXML : byte
    {
        Approved,
        Rejected,
        Done
    }

    public enum FileUploadStatus : byte
    {
        A,
        R,
        D
    }

    public enum UploadFileType : byte
    {
        CSV,
        XML
    }

    public enum TransactionReportType : byte
    {
        ByCurrency,
        ByDate,
        ByStatus
    }

}
