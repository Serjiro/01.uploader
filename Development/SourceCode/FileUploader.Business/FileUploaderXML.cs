﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace FileUploader.Business
{
    public class FileUploaderXML : IProcessFile
    {
        private Stream _stream;
        public FileUploaderXML(Stream stream)
        {
            _stream = stream;
        }

        public List<string> ErrorData { get; private set; }
        public List<UploadData> UploadedList { get; private set; }

        public bool Process()
        {
            var success = true;

            XmlDocument doc = new XmlDocument();
            using (XmlReader reader = XmlReader.Create(_stream))
            {
                if (_stream.Position > 0)
                {
                    _stream.Position = 0;
                }
                doc.Load(_stream);
            }

            XmlNodeList n1 = doc.SelectNodes("Transactions");
            XmlNode root = n1[0];

            ErrorData = new List<string>();
            UploadedList = new List<UploadData>();

            foreach (XmlNode xnode in root.ChildNodes)
            {
                var fileUploadDataProcessor = new FileUploadDataProcessor(xnode);

                if (fileUploadDataProcessor.ErrorList.Count() > 0)
                {
                    ErrorData.AddRange(fileUploadDataProcessor.ErrorList);
                    success = false;
                }
                else
                {
                    if (success && fileUploadDataProcessor.UploadData != null)
                    {
                        UploadedList.Add(fileUploadDataProcessor.UploadData);
                    }
                }
            }


            return success;
        }


    }
}
