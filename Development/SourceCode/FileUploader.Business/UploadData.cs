﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileUploader.Business
{
    public class UploadData
    {
        public string TransactionId { get; set; }
        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }
        public DateTime TransactionDateTime { get; set; }
        public byte Status { get; set; }

    }
}
