﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FileUploader.Business
{
    public interface IProcessFile
    {
        bool Process();

        List<string> ErrorData { get; }
        List<UploadData> UploadedList { get; }
    }
}
