﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileUploader.Business
{
    public class TransactionItem
    {
        //... based on the specs, id and payment are all small letters
        public string id { get; set; }
        public string payment { get; set; }
        public string Status { get; set; }
    }
}
