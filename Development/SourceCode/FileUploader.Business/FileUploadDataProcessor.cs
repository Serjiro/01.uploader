﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml;

namespace FileUploader.Business
{
    public class FileUploadDataProcessor
    {
        public List<string> ErrorList { get; private set; }
        public UploadData UploadData { get; set; }

        //... Common validation for XML and CSV
        //... constructor for the CSV file
        public FileUploadDataProcessor(string[] colFields, string delimeter)
        {
            ProcessValidationsForCSV(colFields, delimeter);
        }

        //... Common validation for XML and CSV
        //... constructor for the CSV file
        public FileUploadDataProcessor(XmlNode xnode)
        {
            ProcessValidationsForXML(xnode);
        }

        private void ProcessValidationsForCSV(string[] colFields, string delimeter)
        {
            ErrorList = new List<string>();
            if (colFields.Length != 5)
            {
                var datastring = string.Join(delimeter, colFields);
                ErrorList.Add($"{SystemMessage.InvalidCSVFile} - {datastring}");
            }
            else
            {
                var transId = colFields[0];
                var amount = colFields[1];
                var currencyCode = colFields[2];
                var trandate = colFields[3];
                var status = colFields[4];

                ProcessValidationsForMandatoryFields(transId, amount, currencyCode, trandate, status);
                if (ErrorList.Count() == 0)
                {
                    ValidateTransactionId(transId);
                    ValidateAmount(amount, out decimal? amountDecimal);
                    ValidateCurrencyCode(currencyCode);
                    ValidateTranDate(trandate, SystemSettings.DateFormatCSVFile, out DateTime? transactionDateTime);
                    var newStatus = ValidatStatusCSV(status);

                    if (ErrorList.Count() == 0)
                    {
                        UploadData = new UploadData
                        {
                            TransactionId = transId,
                            Amount = amountDecimal.Value,
                            CurrencyCode = currencyCode,
                            TransactionDateTime = transactionDateTime.Value,
                            Status = (byte) newStatus
                        };
                    }
                }
            }
        }

        private void ProcessValidationsForXML(XmlNode xnode)
        {
            ErrorList = new List<string>();
            //... check id
            var transIdAttr = xnode.Attributes["id"];
            var transDateNode = xnode.SelectSingleNode("TransactionDate");
            var paymentDetailNode = xnode.SelectSingleNode("PaymentDetails");
            var amountNode = paymentDetailNode?.SelectSingleNode("Amount");
            var currencyCodeNode = paymentDetailNode?.SelectSingleNode("CurrencyCode");
            var statusNode = xnode.SelectSingleNode("Status");

            if (transIdAttr == null || transDateNode == null || amountNode == null || currencyCodeNode == null || statusNode == null)
            {
                ErrorList.Add($"{SystemMessage.InvalidXMLFile} - {xnode}");
            }
            else
            {
                var transId = xnode.Attributes["id"]?.Value;
                var trandate = transDateNode?.InnerText;
                var paymentDetail = xnode.SelectSingleNode("PaymentDetails");
                var amount = paymentDetail?.SelectSingleNode("Amount")?.InnerText;
                var currencyCode = paymentDetail?.SelectSingleNode("CurrencyCode").InnerText;
                var status = xnode.SelectSingleNode("Status").InnerText;

                ProcessValidationsForMandatoryFields(transId, amount, currencyCode, trandate, status);
                if (ErrorList.Count() == 0)
                {
                    ValidateTransactionId(transId);
                    ValidateAmount(amount, out decimal? amountDecimal);
                    ValidateCurrencyCode(currencyCode);
                    ValidateTranDate(trandate, out DateTime? transactionDateTime);
                    var newStatus = ValidatStatusXML(status);

                    if (ErrorList.Count() == 0)
                    {
                        UploadData = new UploadData
                        {
                            TransactionId = transId,
                            Amount = amountDecimal.Value,
                            CurrencyCode = currencyCode,
                            TransactionDateTime = transactionDateTime.Value,
                            Status = (byte)newStatus
                        };
                    }
                }
            }
        }


        //... common/used for both CSV and XML
        private void ProcessValidationsForMandatoryFields(string trans, string amount, string currencycode, string date, string status)
        {
            if (string.IsNullOrEmpty(trans))
            {
                ErrorList.Add(SystemMessage.TransactionIdMandatory);
            }
            if (string.IsNullOrEmpty(amount))
            {
                ErrorList.Add(SystemMessage.AmountMandatory);
            }
            if (string.IsNullOrEmpty(currencycode))
            {
                ErrorList.Add(SystemMessage.CurrencyMandatory);
            }
            if (string.IsNullOrEmpty(date))
            {
                ErrorList.Add(SystemMessage.TransactionDateMandatory);
            }
            if (string.IsNullOrEmpty(status))
            {
                ErrorList.Add(SystemMessage.StatusMandatory);
            }
        }

        public void ValidateTransactionId(string input)
        {
            //... can be put in the config file (if required to put in the config file)
            if (input.Length > 50)
            {
                ErrorList.Add($"{SystemMessage.InvalidTransactionId} {input}");
            }
        }

        public void ValidateTranDate(string input, string format, out DateTime? date)
        {
            date = null;
            if (DateTime.TryParseExact(input, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dt))
            {
                date = dt;
            }
            else
            {
                ErrorList.Add($"{SystemMessage.InvalidDate} {input}");
            }
        }

        public void ValidateTranDate(string input, out DateTime? date)
        {
            date = null;
            if (DateTime.TryParse(input, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dt))
            {
                date = dt;
            }
            else
            {
                ErrorList.Add($"{SystemMessage.InvalidDate} {input}");
            }
        }


        public void ValidateCurrencyCode(string input)
        {
            if (!Tools.ValidCurrencySymbol(input))
            {
                ErrorList.Add($"{SystemMessage.InvalidCurrencyCode} {input}");
            }
        }

        public void ValidateAmount(string input, out Decimal? amount)
        {
            amount = null;
            if (Decimal.TryParse(input, out Decimal d))
            {
                amount = d;
            }
            else
            {
                ErrorList.Add($"{SystemMessage.InvalidAmount} {input}");
            }
        }

        public FileUploadStatusCSV? ValidatStatusCSV(string input)
        {
            if (!Enum.IsDefined(typeof(FileUploadStatusCSV), input))
            {
                ErrorList.Add($"{SystemMessage.InvalidStatus} {input}");
                return null;
            }
            else
            {
                return Enum.Parse<FileUploadStatusCSV>(input);
            }
        }

        public FileUploadStatusXML? ValidatStatusXML(string input)
        {
            if (!Enum.IsDefined(typeof(FileUploadStatusXML), input))
            {
                ErrorList.Add($"{SystemMessage.InvalidStatus} {input}");
                return null;
            }
            else
            {
                return Enum.Parse<FileUploadStatusXML>(input);
            }
        }

    }
}
