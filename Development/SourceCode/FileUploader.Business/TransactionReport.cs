﻿using FileUploader.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FileUploader.Business
{
    public class TransactionReport
    {
        private TransactionContext _context;

        public TransactionReport()
        {
            _context = TransactionContext.GetContext();
        }

        public TransactionReport(TransactionContext context)        
        {
            _context = context;
        }

        public List<TransactionItem> ByCurrency(string currency)
        {
            using (_context)
            {
                return _context.Transactions.Where(t => t.CurrencyCode == currency).ToTransactionItemList();
            }
        }

        public List<TransactionItem> ByDate(DateTime datestart, DateTime dateend)
        {
            using (_context)
            {
                return _context.Transactions.Where(t => t.TransactionDate >= datestart && t.TransactionDate <= dateend)
                    .OrderBy(x => x.TransactionDate)
                    .ToTransactionItemList();
            }
        }

        public List<TransactionItem> ByStatus(FileUploadStatus status)
        {
            byte statusByte = (byte)status;
            using (_context)
            {
                return _context.Transactions.Where(t => t.Status == statusByte).ToTransactionItemList();
            }
        }


    }
}
