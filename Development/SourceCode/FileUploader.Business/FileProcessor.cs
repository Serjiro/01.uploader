﻿using FileUploader.Data;
using FileUploader.Data.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace FileUploader.Business
{
    public class FileProcessor
    {
        public IProcessFile ProcessFile{ get; set; }
        private UploadFileType _uploadType;
        private Stream _stream;
        private string _filename;

        public FileProcessor(string filename, Stream stream)
        {
            _stream = stream;
            _filename = filename;

            var ext = Path.GetExtension(filename).Substring(1).ToUpper();
            Enum.TryParse(ext, out _uploadType);

            switch (_uploadType)
            {
                case UploadFileType.CSV:
                    ProcessFile = new FileUploaderCSV(stream);
                    break;
                case UploadFileType.XML:
                    ProcessFile = new FileUploaderXML(stream);
                    break;
                default:
                    throw new System.NotImplementedException();
            }
        }

        public void Save()
        {
            using (var context = TransactionContext.GetContext())
            {
                foreach (var upload in ProcessFile.UploadedList)
                {
                    var trans = new Transaction
                    {
                        TransactionId = upload.TransactionId,
                        Amount = upload.Amount,
                        CurrencyCode = upload.CurrencyCode,
                        TransactionDate = upload.TransactionDateTime,
                        Status = upload.Status
                    };
                    context.Transactions.Add(trans);
                }
                context.SaveChanges();
            }
        }

        public void SaveError()
        {
            using (var context = TransactionContext.GetContext())
            {
                _stream.Position = 0;
                StreamReader reader = new StreamReader(_stream);
                var transactionError = new TransactionError()
                {
                    Errors = JsonSerializer.Serialize(ProcessFile.ErrorData),
                    FileContent = reader.ReadToEnd(),
                    Filename = _filename,
                    ServerDate = DateTime.Now
                };

                context.TransactionErrors.Add(transactionError);
                context.SaveChanges();
            }
        }

    }
}
