﻿using FileUploader.Data.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace FileUploader.Business
{
    public static class Tools
    {
        static private IEnumerable<string> currencySymbols;

        public static bool ValidCurrencySymbol(string symbol)
        {
            if (currencySymbols == null)
            {
                //... put in memory cache for faster execution
                currencySymbols = CultureInfo.GetCultures(CultureTypes.SpecificCultures)
                    .Select(x => (new RegionInfo(x.LCID)).ISOCurrencySymbol)
                    .Distinct()
                    .OrderBy(x => x);
            };
            return currencySymbols.Contains(symbol);
        }

        public static List<TransactionItem> ToTransactionItemList(this IQueryable<Transaction> query)
        {
            return query.Select(x => new 
                {
                    x.TransactionId, 
                    x.Amount,
                    x.CurrencyCode,
                    x.Status
                })
                .ToArray()
                .Select(x => new TransactionItem
                {
                    id = x.TransactionId,
                    payment = $"{x.Amount.ToString("0.00")} {x.CurrencyCode}",
                    Status = ((FileUploadStatus) x.Status).ToString()
                })                              
                .ToList();
        }
    }
}
