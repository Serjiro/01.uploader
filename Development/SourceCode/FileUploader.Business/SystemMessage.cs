﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileUploader.Business
{
    public static class SystemMessage
    {
        public static string InvalidCSVFile = "Invalid data in the CSV file";
        public static string InvalidXMLFile = "Invalid data in the XML file";
        public static string InvalidTransactionId = "Invalid transaction id";

        public static string TransactionIdMandatory = "Transaction Id is mandatory";
        public static string AmountMandatory = "Amount is mandatory";
        public static string CurrencyMandatory = "Currency Code is mandatory";
        public static string TransactionDateMandatory = "Transaction Date is mandatory";
        public static string StatusMandatory = "Status is mandatory";

        public static string InvalidDate = "Invalid date";
        public static string InvalidCurrencyCode = "Invalid currency code";
        public static string InvalidAmount = "Invalid amount";
        public static string InvalidStatus = "Invalid status";
    }
}
