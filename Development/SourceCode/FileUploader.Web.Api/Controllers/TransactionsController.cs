﻿using System;
using System.Linq;
using FileUploader.Business;
using FileUploader.Data;
using Microsoft.AspNetCore.Mvc;

namespace FileUploader.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {

        [HttpGet]
        [Route("bycurrency/{currency}")]
        public IActionResult ByCurrency(string currency)
        {
            var report = new TransactionReport();
            var result = report.ByCurrency(currency);
            return Ok(result);
        }

        [HttpGet]
        [Route("bydate/{datestart:DateTime}/{dateend:DateTime}")]
        public IActionResult ByDate(DateTime datestart, DateTime dateend)
        {
            var report = new TransactionReport();
            var result = report.ByDate(datestart, dateend);
            return Ok(result);
        }

        [HttpGet]
        [Route("bystatus/{status}")]
        public IActionResult ByStatus(string status)
        {
            if (Enum.TryParse(status, out FileUploadStatus stat))
            {
                var report = new TransactionReport();
                var result = report.ByStatus(stat);
                return Ok(result);
            }

            return NotFound($"{status} is not found");
        }

    }
}
