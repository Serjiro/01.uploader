﻿using FileUploader.Business;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.IO;

namespace FileUploader.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileProcessorController : ControllerBase
    {
        private readonly ILogger<FileProcessorController> _logger;

        public FileProcessorController(ILogger<FileProcessorController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public string Get()
        {
            var baseUrl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";

            var display = $"API ready to accept request at {baseUrl} {Environment.NewLine}{Environment.NewLine}"
                + $"Report Sample URI:{Environment.NewLine}"
                + $"Report by transaction cuurency {baseUrl}/api/transactions/bycurrency/EUR {Environment.NewLine}"
                + $"Report by transaction by date {baseUrl}/api/transactions/bydate/2021-01-01/2021-02-01 {Environment.NewLine}"
                + $"Report by transaction by status {baseUrl}/api/transactions/bystatus/A {Environment.NewLine}";

            return display;
        }

        [HttpPost]
        public IActionResult Post(IFormFile uploadedFile)
        {
            try
            {                
                using (var stream = uploadedFile.OpenReadStream())
                {
                    var fileUploader = new FileProcessor(uploadedFile.FileName, stream);
                    var uploadResult = fileUploader.ProcessFile.Process();

                    if (uploadResult)
                    {
                        fileUploader.Save();

                        return Ok(fileUploader.ProcessFile.UploadedList);
                    }

                    fileUploader.SaveError();
                    return BadRequest(fileUploader.ProcessFile.ErrorData);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
