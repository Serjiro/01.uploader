﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace FileUploader.Business.UnitTest
{
    [TestClass]
    public class RequirementTest_FileUploaderXML
    {
        [TestMethod]
        public void FileUploaderXML_Process_Valid_Test()
        {
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var testfilepath = $"{path}\\testfiles\\testvalid.xml";
            using (var stream = File.OpenRead(testfilepath))
            {
                var fileuploader = new FileUploaderXML(stream);
                var result = fileuploader.Process();

                Assert.IsTrue(result, "process result should be true");
                Assert.IsTrue(fileuploader.ErrorData.Count == 0, "Should contain no erroneous data");
            }
        }

        [TestMethod]
        public void FileUploaderXML_Process_MadatoriesAndValidityOfCSV_Test()
        {
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var testfilepath = $"{path}\\testfiles\\testinvalid1.xml";
            using (var stream = File.OpenRead(testfilepath))
            {
                var fileuploader = new FileUploaderXML(stream);
                var result = fileuploader.Process();

                Assert.IsFalse(result, "process result should be false");
                Assert.IsTrue(fileuploader.ErrorData.Count > 0, "There should be some erroneous data");

                Assert.IsTrue(fileuploader.ErrorData[0].Contains(SystemMessage.InvalidXMLFile), $"Should return {SystemMessage.InvalidXMLFile}");
                Assert.AreEqual(fileuploader.ErrorData[1], SystemMessage.AmountMandatory);
                Assert.AreEqual(fileuploader.ErrorData[2], SystemMessage.CurrencyMandatory);
                Assert.AreEqual(fileuploader.ErrorData[3], SystemMessage.TransactionDateMandatory);
                Assert.AreEqual(fileuploader.ErrorData[4], SystemMessage.StatusMandatory);
            }
        }

        [TestMethod]
        public void FileUploaderXML_Process_SpecificationsAndFormating_Test()
        {
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var testfilepath = $"{path}\\testfiles\\testinvalid2.xml";
            using (var stream = File.OpenRead(testfilepath))
            {
                var fileuploader = new FileUploaderXML(stream);
                var result = fileuploader.Process();

                Assert.IsFalse(result, "process result should be false");
                Assert.IsTrue(fileuploader.ErrorData.Count > 0, "There should be some erroneous data");

                Assert.AreEqual($"{SystemMessage.InvalidTransactionId} 12345678901234567890123456789012345678901234567890X", fileuploader.ErrorData[0]);
                Assert.AreEqual($"{SystemMessage.InvalidAmount} 25A.0", fileuploader.ErrorData[1]);
                Assert.AreEqual($"{SystemMessage.InvalidCurrencyCode} XXX", fileuploader.ErrorData[2]);
                Assert.AreEqual($"{SystemMessage.InvalidDate} 31/31/2025", fileuploader.ErrorData[3]);
                Assert.AreEqual($"{SystemMessage.InvalidStatus} Z", fileuploader.ErrorData[4]);
            }
        }


    }
}
