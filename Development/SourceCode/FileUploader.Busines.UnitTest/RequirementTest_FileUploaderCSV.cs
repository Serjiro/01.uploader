using FileUploader.Business;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.IO;
using System.Reflection;

namespace FileUploader.Business.UnitTest
{
    [TestClass]
    public class RequirementTest_FileUploaderCSV
    {
        [TestMethod]
        public void FileUploaderCSV_Process_Valid_Test()
        {
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var testfilepath = $"{path}\\testfiles\\testvalid.csv";
            using (var stream = File.OpenRead(testfilepath))
            {
                var fileuploader = new FileUploaderCSV(stream);
                var result = fileuploader.Process();

                Assert.IsTrue(result, "process result should be true");
                Assert.IsTrue(fileuploader.ErrorData.Count == 0, "Should contain no erroneous data");
            }
        }

        [TestMethod]
        public void FileUploaderCSV_Process_MadatoriesAndValidityOfCSV_Test()
        {
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var testfilepath = $"{path}\\testfiles\\testinvalid1.csv";
            using (var stream = File.OpenRead(testfilepath))
            {
                var fileuploader = new FileUploaderCSV(stream);
                var result = fileuploader.Process();

                Assert.IsFalse(result, "process result should be false");
                Assert.IsTrue(fileuploader.ErrorData.Count > 0, "There should be some erroneous data");

                Assert.IsTrue(fileuploader.ErrorData[0].Contains(SystemMessage.InvalidCSVFile), $"Should return {SystemMessage.InvalidCSVFile}");
                Assert.AreEqual(SystemMessage.TransactionIdMandatory, fileuploader.ErrorData[1]);
                Assert.AreEqual(SystemMessage.AmountMandatory, fileuploader.ErrorData[2]);
                Assert.AreEqual(SystemMessage.CurrencyMandatory, fileuploader.ErrorData[3]);
                Assert.AreEqual(SystemMessage.TransactionDateMandatory, fileuploader.ErrorData[4]);
                Assert.AreEqual(SystemMessage.StatusMandatory, fileuploader.ErrorData[5]);
            }
        }

        [TestMethod]
        public void FileUploaderCSV_Process_SpecificationsAndFormating_Test()
        {
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var testfilepath = $"{path}\\testfiles\\testinvalid2.csv";
            using (var stream = File.OpenRead(testfilepath))
            {
                var fileuploader = new FileUploaderCSV(stream);
                var result = fileuploader.Process();

                Assert.IsFalse(result, "process result should be false");
                Assert.IsTrue(fileuploader.ErrorData.Count > 0, "There should be some erroneous data");

                Assert.AreEqual($"{SystemMessage.InvalidTransactionId} 12345678901234567890123456789012345678901234567890X", fileuploader.ErrorData[0]);
                Assert.AreEqual($"{SystemMessage.InvalidAmount} 25A.0", fileuploader.ErrorData[1]);
                Assert.AreEqual($"{SystemMessage.InvalidCurrencyCode} XXX", fileuploader.ErrorData[2]);
                Assert.AreEqual($"{SystemMessage.InvalidDate} 31/31/2025", fileuploader.ErrorData[3]);
                Assert.AreEqual($"{SystemMessage.InvalidStatus} Z", fileuploader.ErrorData[4]);
            }
        }


    }
}
