﻿using FileUploader.Data;
using FileUploader.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;

namespace FileUploader.Business.UnitTest
{
    [TestClass]
    public class RequirementReportTest
    {
        [TestMethod]
        public void Test_TransactionReport_ByCurrency()
        {
            var transaction = new Transaction[]
            {
                new Transaction(){TransactionId = "11111", Amount = 20.5M, CurrencyCode ="EUR", Status = 0, TransactionDate = new DateTime(2021,12,06) },
                new Transaction(){TransactionId = "11112", Amount = 10.2M, CurrencyCode ="USD", Status = 1, TransactionDate = new DateTime(2021,12,07) },
                new Transaction(){TransactionId = "11113", Amount = 10.2M, CurrencyCode ="EUR", Status = 2, TransactionDate = new DateTime(2021,12,08) },
            }.AsQueryable();

            var mockContext = new Mock<TransactionContext>();
            mockContext.Setup(c => c.Transactions).Returns(MockDbSet(transaction).Object);

            var transactionReport = new TransactionReport(mockContext.Object);
            var result = transactionReport.ByCurrency("EUR");

            Assert.IsTrue(result.Count == 2, "Total result should be 2");
            Assert.AreEqual("11111", result[0].id);
            Assert.AreEqual("20.50 EUR", result[0].payment);
            Assert.AreEqual("A", result[0].Status);
        }

        [TestMethod]
        public void Test_TransactionReport_ByDate()
        {
            var transaction = new Transaction[]
            {
                new Transaction(){TransactionId = "11111", Amount = 20.5M, CurrencyCode ="EUR", Status = 0, TransactionDate = new DateTime(2021,12,06) },
                new Transaction(){TransactionId = "11112", Amount = 10.2M, CurrencyCode ="USD", Status = 1, TransactionDate = new DateTime(2021,12,07) },
                new Transaction(){TransactionId = "11113", Amount = 13.25M, CurrencyCode ="EUR", Status = 2, TransactionDate = new DateTime(2021,12,08) },
            }.AsQueryable();

            var mockContext = new Mock<TransactionContext>();
            mockContext.Setup(c => c.Transactions).Returns(MockDbSet(transaction).Object);

            var transactionReport = new TransactionReport(mockContext.Object);
            var result = transactionReport.ByDate(new DateTime(2021, 12, 07), new DateTime(2021, 12, 08));

            Assert.IsTrue(result.Count == 2, "Total result should be 2");
            Assert.AreEqual("11112", result[0].id);
            Assert.AreEqual("10.20 USD", result[0].payment);
            Assert.AreEqual("R", result[0].Status);
        }

        [TestMethod]
        public void Test_TransactionReport_ByStatus()
        {
            var transaction = new Transaction[]
            {
                new Transaction(){TransactionId = "11111", Amount = 20.5M, CurrencyCode ="EUR", Status = 0, TransactionDate = new DateTime(2021,12,06) },
                new Transaction(){TransactionId = "11112", Amount = 10.2M, CurrencyCode ="USD", Status = 1, TransactionDate = new DateTime(2021,12,07) },
                new Transaction(){TransactionId = "11113", Amount = 13.25M, CurrencyCode ="EUR", Status = 2, TransactionDate = new DateTime(2021,12,08) },
            }.AsQueryable();

            var mockContext = new Mock<TransactionContext>();
            mockContext.Setup(c => c.Transactions).Returns(MockDbSet(transaction).Object);

            var transactionReport = new TransactionReport(mockContext.Object);
            var result = transactionReport.ByStatus(FileUploadStatus.D);

            Assert.IsTrue(result.Count == 1, "Total result should be 2");
            Assert.AreEqual("11113", result[0].id);
            Assert.AreEqual("13.25 EUR", result[0].payment);
            Assert.AreEqual("D", result[0].Status);
        }


        private Mock<DbSet<T>> MockDbSet<T>(IQueryable<T> data) where T : class
        {
            var mockSet = new Mock<DbSet<T>>();
            mockSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());
            return mockSet;
        }
    }
}
