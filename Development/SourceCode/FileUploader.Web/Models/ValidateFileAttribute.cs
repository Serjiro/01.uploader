﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace FileUploader.Web.Models
{
    public class ValidateFileAttribute : RequiredAttribute
    {
        public override bool IsValid(object value)
        {
            var file = value as IFormFile;

            if (file == null || !ValidateFilename(file.FileName))
            {
                ErrorMessage = "Unknown format";
                return false;
            }
            else if (!ValidateFileSize(file.Length))
            {
                ErrorMessage = "File size is max 1 MB";
                return false;
            }

            return true ;
        }

        private static bool ValidateFilename(string filename)
        {
            var ext = Path.GetExtension(filename).Substring(1).ToUpper();
            return Enum.IsDefined(typeof(UploadFileType), ext);
        }

        private static bool ValidateFileSize(long filesize)
        {
            long maximumFileSize = 1 * 1024 * 1024; //... should be in the config
            return filesize <= maximumFileSize;
        }

        private enum UploadFileType : byte
        {
            CSV,
            XML
        }


    }
}
