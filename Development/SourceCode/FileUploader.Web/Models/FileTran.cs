﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FileUploader.Web.Models
{
    public class FileTran
    {
        [ValidateFile]
        public IFormFile UploadFile { get; set; }
    }
}
