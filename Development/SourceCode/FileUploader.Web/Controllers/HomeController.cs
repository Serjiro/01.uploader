﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using FileUploader.Web.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace FileUploader.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _configuration;

        public HomeController(ILogger<HomeController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;            
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(FileTran fileTran)
        {
            if (!ModelState.IsValid)
            {
                return View(fileTran);
            }

            try
            {
                byte[] fileBytes;
                using (var ms = new MemoryStream())
                {
                    fileTran.UploadFile.CopyTo(ms);
                    fileBytes = ms.ToArray();                    
                }

                //... separation of concern - business layer api should not be exposed to public for security reasons and best practice.
                //... create an api as a microservice
                var response = WebTools.Tools.GetResponseFromUploaderApi(_configuration["UploaderApiUrl"], fileTran.UploadFile.FileName, fileBytes);

                return Json(new { status = response.StatusCode, response = response.Content});

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return View(fileTran);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
