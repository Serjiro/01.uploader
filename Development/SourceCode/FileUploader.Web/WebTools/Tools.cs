﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace FileUploader.Web.WebTools
{
    public class Tools
    {
        public static IRestResponse GetResponseFromUploaderApi(string serviceUrl, string filename, byte[] fileContent)
        {
            var client = new RestClient(serviceUrl);

            var request = new RestRequest(Method.POST);
            request.AddFile("uploadedFile", fileContent, filename);
            request.AlwaysMultipartFormData = true;
            request.AddHeader("cache-control", "no-cache,no-cache");

            IRestResponse response = client.Execute(request);

            return response;
        }
    }
}
