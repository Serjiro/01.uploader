
IF OBJECT_ID('[dbo].[Transactions]') IS NULL
BEGIN
	CREATE TABLE [dbo].[Transactions](
		[TransactionId] [varchar](50) NOT NULL,
		[TransactionDate] [datetime] NOT NULL,
		[CurrencyCode] [varchar](3) NOT NULL,
		[Amount] [decimal](14, 2) NOT NULL,
		[Status] [tinyint] NOT NULL
	) ON [PRIMARY]
END
GO

IF OBJECT_ID('[dbo].[TransactionErrors]') IS NULL
BEGIN
	CREATE TABLE [dbo].[TransactionErrors](
		[TransactionErrorId] [int] IDENTITY(1,1) NOT NULL,
		[Filename] [nvarchar](200) NOT NULL,
		[FileContent] [nvarchar](max) NOT NULL,
		[Errors] [nvarchar](max) NOT NULL,
		[ServerDate] [datetime] NOT NULL,
	 CONSTRAINT [PK_TransactionErrors] PRIMARY KEY CLUSTERED 
	(
		[TransactionErrorId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
